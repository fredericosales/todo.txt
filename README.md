## Todo.txt
---

#### If you want to get it done, first write it down.

> If you have a file called todo.txt on your computer right now, you're in the right place.


So many power users try dozens of complicated todo list software applications, only to go right back to their trusty todo.txt file.

But it's not easy to open todo.txt, make a change, and save it—especially on your touchscreen device and at the command line. Todo.txt apps solve that problem.

> Simplicity is todo.txt's core value.

You're not going to find many checkboxes, drop-downs, reminders, or date pickers here.

Todo.txt apps are minimal, todo.txt-focused editors which help you manage your tasks with as few keystrokes and taps possible.

#### At the Command Line

With a simple but powerful shell script called todo.sh, you can interact with todo.txt at the command line for quick and easy, Unix-y access.

[Todo.txt CLI >>](https://github.com/todotxt/todo.txt-cli/releases)

#### Find out more

* [Documentation](https://github.com/todotxt/todo.txt-cli/wiki/User-Documentation) - everything you need to know about how to use Todo.txt CLI
* [Gitter](https://gitter.im/todotxt/Lobby) - ask the Todo.txt community

#### Developers

Todo.txt CLI and Todo.txt Touch are proudly open source. Browse the source code for the [CLI](https://github.com/todotxt/todo.txt-cli), [iOS](https://github.com/todotxt/todo.txt-ios), and [Android](https://github.com/todotxt/todo.txt-android).


### Plugins and Add-ons

#### [Vim plugin for todo.txt](https://github.com/freitass/todo.txt-vim)

* By Leandro Freitas.

#### [Todo.sh Add-on Directory](https://github.com/todotxt/todo.txt-cli/wiki/Todo.sh-Add-on-Directory)

* A collection of add-ons, custom actions, and filters that enhance the Todo.txt CLI script, authored by community members.
	* By Cthulhux.

#### Sublime Text todo.txt syntax highlighting.

* By Cthulhux.

#### [Todo.txt Thunderbird Extension](https://addons.mozilla.org/en-US/thunderbird/addon/todotxt-extension/)

* By Roy Kokkelkoren.

#### [Atom plugin for todo.txt](https://atom.io/packages/language-todotxt)

* A plugin to make editing your todo.txt in Atom a breeze.

#### [VSCode plugin for todo.txt](https://github.com/vamanos/todo-txt)

* Todo.txt syntax highlighter and helper extension for visual studio code.

#### [Gedit highlighter for todo.txt](https://github.com/edusantana/todo-txt-gedit-highlight)

* Todo.txt syntax highlighter for gedit.

#### GNOME Shell Extension

* A Gnome shell interface for todo.txt. [Source](https://gitlab.com/bartl/todo-txt-gnome-shell-extension)

### Web

#### [todoTxtWebUi](https://github.com/bicarbon8/todoTxtWebUi)

* A web UI to use with a todo.txt file [demo](https://chrome.google.com/webstore/detail/mndijfcodpjlhgjcpcbhncjakaboedbl).
	* By Jason Holt.

#### [Todo.txt for Chrome](https://chrome.google.com/webstore/detail/mndijfcodpjlhgjcpcbhncjakaboedbl)

* Chrome extension with Dropbox integration, and features such as pending task count, saved filters, and more.
	* By Aditya Bhaskar.

---
